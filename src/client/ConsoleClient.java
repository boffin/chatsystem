package client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Scanner;

public class ConsoleClient {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		final ClientProgram client = new ClientProgram();

		if (client.connect(new InetSocketAddress("localhost", 8080))) {
			System.out.println("Connected");
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						if (( client.getFirstMessage()) != null) {
						} else {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// Arbitrary catch for thread.
							}
						}
					}
				}
			}).start();
			Scanner input = new Scanner(System.in);
			String line;
			while (!(line = input.nextLine()).equals("quit")) {
				if (line.startsWith("@")) {
					client.changeNick(line.substring(1));
				} else {
					client.sendMessage(line);
				}
			}
			input.close();
		}

	}

}
