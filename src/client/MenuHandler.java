package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * MenuHandler.java
 *
 * The MenuHandler deals primarily with how the client reacts to commands sent
 * in by the user via the GUI. This is primarily done by calling functions in
 * the client program.
 */

public class MenuHandler implements ActionListener{

	private final int QUIT = 0;
	private final int CONNECT = 1;
	private final int DISCONNECT = 2;
	private final int SEND = 3;
	private final int CHANGENICK = 4;
	private final int CHANGETOPIC = 5;
	private final int SETTINGS = 6;
	private final int WHOIS = 7;

	private int option;
	private GUI gui;

	public MenuHandler(int i, GUI gui) {
		option = i;
		this.gui = gui;
	}

	/**
	 * Acts as a button listener and will react to whatever GUI component is triggered.
	 * @param arg0 - Action activated in the GUI.
	 * @return
	 * @throws IOException
	 */

	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch (option) {
		//quit button
		case QUIT:
			try {
				gui.client.quit();
			} catch (IOException e2) {
				JOptionPane.showMessageDialog(gui, "Problem quitting server!");
			}
			System.exit(0);
			break;
			// Connect button
		case CONNECT:
			try {
				gui.client.fetchList();
			} catch (IOException e5) {
				JOptionPane.showMessageDialog(gui, "Problem fetching serverlist!");
			}
			ArrayList<ServerID> servList= gui.client.getServers();
			ServerID[] servers = new ServerID[servList.size()];
			ServerID chosen = null;
			for (int i = 0; i < servList.size(); i++) {
				servers[i] = servList.get(i);
			}
			chosen = (ServerID) JOptionPane.showInputDialog(
					gui,
					"Please pick a server:",
					"Server List",
					JOptionPane.PLAIN_MESSAGE,
					null,
					servers,
					"C");

			if (chosen != null) {
				try {
					gui.client.connect(new InetSocketAddress(chosen.address, chosen.portNr));
					gui.client.register();
					JOptionPane.showMessageDialog(gui,"The topic is:\n" + chosen.topicName,"Connected to " + chosen.hostName, JOptionPane.INFORMATION_MESSAGE);
				} catch (IOException e3) {
					JOptionPane.showMessageDialog(gui, "Problem fetching serverlist!");
				}
			}
			break;
			// Disconnect button
		case DISCONNECT:
			try {
				gui.client.quit();
				gui.client.disconnect();
			} catch (IOException e2) {
				JOptionPane.showMessageDialog(gui, "Problem disconnecting!");
			}
			break;
			//Send Button
		case SEND:
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						gui.client.sendMessage(gui.getMessage());
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(gui, "Unable to send to server!");
					}
				}
			}).start();
			break;
			//Change Nick
		case CHANGENICK:
			String i = JOptionPane.showInputDialog(null,
					"Enter new nickname!");
			if (i != null) {
				try {
					gui.client.changeNick(i);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(gui, "Problem changing nickname!");
				}
			}
			break;
			//Change topic
		case CHANGETOPIC:
			String j = JOptionPane.showInputDialog(null,
					"Enter new topic!");
			if (j != null) {
				try {
					gui.client.changeTopic(j);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(gui, "Problem changing topic!");
				}
			}
			break;
			//Change settings
		case SETTINGS:
			new SettingsDialog(gui, gui.client);
			break;
			//Retrieve user data
		case WHOIS:
			if (gui.getUser() != null) {
				try {
					gui.client.getUserData(gui.getUser());
				} catch (IOException e) {
					JOptionPane.showMessageDialog(gui, "Problem getting user data!");
				}
			}
			break;
		default:
			break;
		}
	}
}
