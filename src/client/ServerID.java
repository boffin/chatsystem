package client;

import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;

/**
 * ServerID.java Datatype for server information.
 *
 * @version 1.00
 */

public class ServerID {
	public String topicName;
	public int portNr;
	public int nrOfClients;
	public String address;
	private InetSocketAddress ip;
	public String hostName;

	public ServerID(short[] address ,byte[] topicName,int port, int clients ) throws UnsupportedEncodingException {

		this.topicName = new String(topicName,"UTF-8");

		portNr = port;
		nrOfClients = clients;
		String res = "";
		for (int i = 0; i < address.length; i++) {
			res += address[i] + ".";
		}
		res = res.substring(0, res.length() - 1);
		this.address = res;
		ip = new InetSocketAddress(this.address, portNr);
		hostName = ip.getHostName();
	}

	public String toString() {
		return "[" + nrOfClients + "/255] " + hostName;
	}
}
