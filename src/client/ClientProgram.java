package client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.zip.GZIPInputStream;
import client.Commands;

import common.Checksum;
import common.Crypt;
import common.MsgTypes;
import common.PDU;
import common.OpCodes;
import common.UDPConnection;

/**
 * ClientProgram.java
 *
 * Client program is the class where most of the client side intelligence is located.
 * It contains methods that pertain both to handling incoming data and sending PDU's
 * created by the Commands class.
 */
public class ClientProgram {

	private String nick;
	private SocketChannel socketChannel;
	private ArrayList<ServerID> servers;
	private Queue<String> messages;
	private Vector<String> nickList;
	private boolean nicksChanged = false;
	private boolean encryption = false;
	private boolean compression = false;
	private String encryptionKey = "foobar";

	private UDPConnection nameServer;
	private String nameServerAddress = "itchy.cs.umu.se";
	private int nameServerPort = 1337;
	private String lastSearch;

	public ClientProgram() throws IOException {
		nameServer = new UDPConnection(nameServerAddress, nameServerPort);
		nick = "default";
		messages = new ConcurrentLinkedQueue<String>();
		nickList = new Vector<String>();
		socketChannel = SocketChannel.open();
		servers = new ArrayList<ServerID>();
		new Thread(listenLoop()).start();
	}

	/**
	 * Tries to connect to a chat server at the given address. May block the calling thread if
	 * the connection cannot be established immediately.
	 * @param remote The address of the chat server
	 * @return True upon successful connection, false otherwise
	 * @throws IOException
	 */
	public boolean connect(SocketAddress remote) throws IOException {
		boolean connected = false;
		if (!socketChannel.isOpen()) {
			socketChannel = SocketChannel.open();
		}
		if (socketChannel.isConnected()) {
			messages.add("			ALREADY CONNECTED			");
		}else{
			try {
				connected = socketChannel.connect(remote);
			} catch (ConnectException e) {
				connected = false;
			}
		}

		return connected;
	}

	/**
	 * Closes the socket to the chat server
	 * @throws IOException
	 */
	public void disconnect() throws IOException {
		if (socketChannel.isConnected()) {
			nickList.clear();
			nicksChanged = true;
			messages.add("Disconnected from server.");
			socketChannel.close();
		}
	}

	/**
	 * Sends the given byte array to the server.
	 * @param msg A byte array containing the command to send.
	 * @throws IOException
	 */
	private void send(byte[] msg) throws IOException {
		if (socketChannel.isConnected()) {
			ByteBuffer buffer = ByteBuffer.wrap(msg);
			while (buffer.hasRemaining()) {
				socketChannel.write(buffer);
			}
		}
	}

	//	/**
	//	 * Use this version of send to test various packet sizes and delays
	//	 */
	//	public void send(byte[] msg) throws IOException {
	//
	//		final int SIZE = 30;
	//
	//		if (socketChannel.isConnected()) {
	//			int bytes = Math.min(msg.length, SIZE);
	//			for (int pos = 0; pos < msg.length; pos += bytes) {
	//				byte[] temp = new byte[bytes];
	//				System.arraycopy(msg, pos, temp, 0, Math.min(msg.length-pos,bytes));
	//				ByteBuffer buffer = ByteBuffer.wrap(temp);
	//				while (buffer.hasRemaining()) {
	//					socketChannel.write(buffer);
	//				}
	//				try {
	//					Thread.sleep(300);
	//				} catch (InterruptedException e) {
	//					// TODO Auto-generated catch block
	//					e.printStackTrace();
	//				}
	//			}
	//		}
	//	}

	/**
	 * Generates a Runnable to be started in its own thread. Handles all incoming information
	 * from the chat server connection and reacts accordingly.
	 * @return
	 */
	private Runnable listenLoop() {
		return new Runnable() {
			@Override
			public void run() {
				ByteBuffer bb = ByteBuffer.allocate(150000);
				while (true) {
					//Sleeps while no connection is available
					if (!socketChannel.isConnected()) {
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							messages.add("				CONNECTION INTERRUPTED				");
						}
						continue;
					}

					bb.clear();
					try {
						int bytesRead = socketChannel.read(bb);
						if (bytesRead != -1) {
							bb.flip();
							while (bb.hasRemaining()) {
								bb.mark();
								byte op = bb.get();
								switch (op) {
								case OpCodes.MESSAGE:
									receiveMessage(bb);
									break;
								case OpCodes.UCNICK:
									userChangedNick(bb);
									break;
								case OpCodes.NICKS:
									nickList(bb);
									break;
								case OpCodes.UJOIN:
									userJoinedServer(bb);
									break;
								case OpCodes.ULEAVE:
									userLeftServer(bb);
									break;
								case OpCodes.CHTOPIC:
									topicChanged(bb);
									break;
								case OpCodes.UINFO:
									printUserInfo(bb);
									break;
								}
							}
							//If multiple commands are sent in the
							//same packet the buffer is repositioned
							//to right where the next command starts.
							toNextCommand(bb);
						}else {
							messages.add("			CONNECTION TO SERVER LOST				");
							socketChannel.close();
						}
					} catch (AsynchronousCloseException e) {
						//This exception happens when closing connection to
						//server and is harmless.
					} catch (IOException e) {
						e.printStackTrace();
					} catch (BufferUnderflowException e) {
						//happens when a command is shorter than it should
						e.printStackTrace();
					}
				}
			}
		};
	}

	/**
	 * Gets list of chat servers from name server by UDP. First sends command then
	 * waits for response and parses it.
	 * @return True on correctly received server list.
	 * @throws IOException
	 */
	public Boolean fetchList () throws IOException{
		nameServer = new UDPConnection(nameServerAddress, nameServerPort);
		servers.removeAll(servers);
		byte[] getListCmd = Commands.getList();
		byte[] response = nameServer.send(getListCmd);
		if (response != null) {
			PDU pdu = new PDU(response, response.length);
			short op = pdu.getByte(0);
			if (op == OpCodes.SLIST) {
				while (!parser(pdu)){
					response = nameServer.send(getListCmd);
					pdu = new PDU(response, response.length);
				};
				return true;
			} else {
				messages.add("			NAME SERVER UNABLE TO RESPOND			");
			}
		} else {
			messages.add("			UNABLE TO CONNECT TO NAME SERVER			");
		}
		return false;
	}

	/**
	 * Helper function for fetchList to parse the server list.
	 * @param pdu Contains the server list
	 * @return True if the expected number of servers could be parsed, false otherwise.
	 * @throws UnsupportedEncodingException
	 */
	private Boolean parser(PDU pdu) throws UnsupportedEncodingException {
		@SuppressWarnings("unused")
		short seqNr = pdu.getByte(1);
		//Compare seqnr to expected?

		int nrOfServers = pdu.getShort(2);
		int offset = 4;

		try {
			for (int i = 0; i < nrOfServers; i++) {
				int tpcLen =  pdu.getByte(offset+7);
				short[] address = new short[4];
				address[0] = pdu.getByte(offset);
				address[1] = pdu.getByte(offset+1);
				address[2] = pdu.getByte(offset+2);
				address[3] = pdu.getByte(offset+3);

				byte[] topicName = pdu.getSubrange(offset+8,tpcLen);

				int portNr = pdu.getShort(offset+4);
				int nrOfClients = (int)pdu.getByte(offset+6);

				servers.add(new ServerID(address,topicName, portNr, nrOfClients));
				offset = offset+8+tpcLen+Commands.calcPadding(offset+8+tpcLen);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return true;
	}

	/**
	 * Responds to a topic change by adding it to the message queue.
	 * @param bb ByteBuffer containing the information
	 * @throws UnsupportedEncodingException
	 */
	private void topicChanged(ByteBuffer bb) throws UnsupportedEncodingException {
		short tpcLen = Commands.getUnsigned(bb.get());
		bb.getShort();
		byte[] topicBytes = new byte[tpcLen];
		bb.get(topicBytes);
		String topic = new String(topicBytes, "UTF-8");
		messages.add("\n\n		TOPIC CHANGED TO "+topic+"!		\n\n");
	}

	private void printUserInfo(ByteBuffer bb) throws UnsupportedEncodingException {
		bb.get();
		bb.get();
		bb.get();

		int[] addr = new int[4];
		addr[0] = Commands.getUnsigned(bb.get());
		addr[1] = Commands.getUnsigned(bb.get());
		addr[2] = Commands.getUnsigned(bb.get());
		addr[3] = Commands.getUnsigned(bb.get());

		String res = "";
		for (int i = 0; i < addr.length; i++) {
			res += addr[i] + ".";
		}
		res = res.substring(0, res.length() - 1);
		InetSocketAddress ip = new InetSocketAddress(res, 8080);
		String name = ip.getHostName();
		int joined = (int) bb.getInt();

		Date time = new Date(joined*1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeString = sdf.format(time);
		messages.add("At [" + timeString + "] client "+lastSearch+" at address " + name +" joined the server.");
	}

	/**
	 * Responds to message about a new user joining the chat server.
	 * @param bb ByteBuffer containing the information
	 * @throws UnsupportedEncodingException
	 */
	private void userJoinedServer(ByteBuffer bb) throws UnsupportedEncodingException {
		short nickLength = Commands.getUnsigned(bb.get());
		bb.get(); //Padding
		bb.get();
		int timeStamp = bb.getInt();
		byte[] nickBytes = new byte[nickLength];
		bb.get(nickBytes);
		String nick = new String(nickBytes, "UTF-8");

		if (!nickList.contains(nick)) {
			Date time = new Date(timeStamp*1000L);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String timeString = sdf.format(time);
			messages.add("[" + timeString + "] " + nick + " has joined the server.");
			nickList.add(nick);
			//Sets a flag to notify the gui that it needs to update the nick list.
			nicksChanged = true;
		}
	}

	/**
	 * Responds to a user leaving the server.
	 * @param bb ByteBuffer containing the information
	 * @throws UnsupportedEncodingException
	 */
	private void userLeftServer(ByteBuffer bb) throws UnsupportedEncodingException {
		short nickLength = Commands.getUnsigned(bb.get());
		bb.get(); //Padding
		bb.get();
		int timeStamp = bb.getInt();
		byte[] nickBytes = new byte[nickLength];
		bb.get(nickBytes);
		String nick = new String(nickBytes, "UTF-8");
		Date time = new Date(timeStamp*1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeString = sdf.format(time);
		messages.add("[" + timeString + "] " + nick + " has left the server.");

		for (int i = 0; i < nickList.size(); i++) {
			if (nickList.get(i).equals(nick)) {
				nickList.remove(i);
				//Sets a flag to notify the gui that it needs to update the nick list.
				nicksChanged = true;
				break;
			}
		}
	}

	/**
	 * Changes nickname and notifies the chat server.
	 * @param nick the nickname to change to.
	 * @throws IOException
	 */
	public void changeNick(String nick) throws IOException {
		this.nick = nick;
		byte[] cmd = Commands.changeNick(nick);
		send(cmd);
	}

	/**
	 * Changes the topic in the chat server.
	 * @param topic the topic to change to.
	 * @throws IOException
	 */
	public void changeTopic(String topic) throws IOException {
		byte[] cmd = Commands.changeTopic(topic);
		send(cmd);
	}

	/**
	 * Registers at the chat server. Needs to be done before any
	 * other commands or else they will be discarded by the
	 * chat server.
	 * @throws IOException
	 */
	public void register() throws IOException {
		byte[] cmd = null;
		try {
			cmd = Commands.register(nick);
		} catch (UnsupportedEncodingException e) {
			// Arbitrary missing library exception.
		}
		send(cmd);
	}

	/**
	 * Sends quit command to chat server.
	 * @throws IOException
	 */
	public void quit() throws IOException {
		byte[] cmd = Commands.quit();
		send(cmd);
	}

	/**
	 * Sends text message to chat server. The global flags 'encryption' and 'compression'
	 * are used to specify if encryption and/or compression should be enabled.
	 * @param message string containing the message.
	 * @throws IOException
	 */
	public void sendMessage(String message) throws IOException {

		byte[] cmd = Commands.sendMessage(message, nick, encryption, compression, encryptionKey);
		send(cmd);
	}

	/**
	 * Responds to nickname changes by other users.
	 * @param buffer
	 * @throws UnsupportedEncodingException
	 */
	private void userChangedNick(ByteBuffer buffer) throws UnsupportedEncodingException {

		short oldLength = Commands.getUnsigned(buffer.get());
		short newLength = Commands.getUnsigned(buffer.get());
		buffer.get();
		int timeStamp = buffer.getInt();
		byte[] oldBytes = new byte[oldLength];
		byte[] newBytes = new byte[newLength];
		buffer.get(oldBytes);
		//The length is divisible by 4 so the buffer needs to be
		//moved ahead depending on how long the data is.
		int padding = Commands.calcPadding(oldLength);
		for (int i = 0; i < padding; i++) {
			buffer.get();
		}
		buffer.get(newBytes);
		String oldNick = new String(oldBytes, "UTF-8");
		String newNick = new String(newBytes, "UTF-8");
		Date time = new Date(timeStamp*1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeString = sdf.format(time);
		messages.add("[" + timeString + "] " + oldNick + " changed name to " + newNick + ".");
		//Finds the user in the nick list and updates its name.
		for (int i = 0; i < nickList.size(); i++) {
			if (nickList.get(i).equals(oldNick)) {
				nickList.set(i, newNick);
				//Sets a flag to notify the gui that it needs to update the nick list.
				nicksChanged = true;
				break;
			}
		}
	}

	/**
	 * Sometimes multiple commands get stored in the same buffer. In those
	 * cases the buffer needs to be repositioned so it can be used again.
	 * It reads until the buffer ends or something other than 0 is found
	 * and then marks that as the start position.
	 * @param buffer the ByteBuffer to be repositioned
	 */
	private void toNextCommand(ByteBuffer buffer) {
		while (buffer.hasRemaining() && buffer.get() == 0);
		if (buffer.hasRemaining()) {
			buffer.position(buffer.position()-1);
			buffer.compact();
			buffer.flip();
		}
	}

	/**
	 * Gets list of connected users from the chat server.
	 * @param buffer ByteBuffer containing the information
	 */
	private void nickList(ByteBuffer buffer) {
		short numberOfNicks = Commands.getUnsigned(buffer.get());
		int totLength = Commands.getUnsigned(buffer.getShort());
		byte[] nickBytes = new byte[totLength];
		buffer.get(nickBytes);

		nickList.clear();
		int start = 0;

		//Tries to find the specified number of strings in the byte array
		//by looking for null bytes and extracting the characters leading up to it.
		//The start position is then moved up to right after the null byte and
		//the process is repeated.
		for (int i = 0; i < numberOfNicks; i++) {
			int length = 0;
			int pos = start;
			//Tries to find the bounds of the current string
			while (pos < nickBytes.length && nickBytes[pos] != 0) {
				length++;
				pos++;
			}

			try {
				String nick = new String(nickBytes,start,length,"UTF-8");
				nickList.add(nick);
			} catch (UnsupportedEncodingException e) {
				//Arbitrary missing file exception.
			}
			start = pos + 1;
		}
		//Sets flag to notify gui about the change
		nicksChanged = true;
	}

	/**
	 * Parses a text message command and decompresses it if needed.
	 * @param buffer ByteBuffer containing the command
	 * @throws UnsupportedEncodingException
	 */
	private void receiveMessage(ByteBuffer buffer) throws UnsupportedEncodingException {
		byte msgType = buffer.get();
		short nickLen = Commands.getUnsigned(buffer.get());
		byte checkSum = buffer.get();
		int msgLen = Commands.getUnsigned(buffer.getShort());
		buffer.getShort(); //padding
		int timeStamp = buffer.getInt(); //should be 0
		byte[] msg = new byte[msgLen];
		buffer.get(msg);

		int padding = Commands.calcPadding(msgLen);
		for (int i = 0; i < padding; i++) {
			buffer.get();
		}
		byte[] nick = new byte[nickLen];
		buffer.get(nick);

		buffer.reset();
		byte[] cmd = new byte[12+nickLen+msgLen+Commands.calcPadding(msgLen)];
		buffer.get(cmd);

		int newChecksum = Checksum.calc(cmd, cmd.length);
		if (0 != newChecksum) {
			messages.add("Checksum mismatch from " + new String(nick,"UTF-8"));
			return;
		}

		if ((msgType & MsgTypes.CRYPT) > 0) {
			PDU pdu = new PDU(msg, msg.length);
			pdu.getByte(0);
			checkSum = (byte) pdu.getByte(1); //checksum
			int cryptLen = pdu.getShort(2); //compressed length
			pdu.getShort(4); //uncompressed length
			byte[] cryptMsg = pdu.getSubrange(8, cryptLen);

			if (0 != Checksum.calc(pdu.getBytes(), pdu.length())) {
				messages.add("Checksum mismatch when decrypting");
				return;
			} else {
				//				System.out.println("Checksum ok");
			}
			if (encryptionKey.length() != 0) {
				byte[] key = encryptionKey.getBytes("UTF-8");
				Crypt.decrypt(cryptMsg, cryptLen, key, key.length);
			}
			msg = cryptMsg;
		}
		if ((msgType & MsgTypes.COMP) > 0) {
			PDU pdu = new PDU(msg, msg.length);
			pdu.getByte(0);
			checkSum = (byte)pdu.getByte(1); //checksum
			int compLen = pdu.getShort(2); //compressed length
			pdu.getShort(4); //uncompressed length
			byte[]compMsg = null;
			try {
				compMsg = pdu.getSubrange(8, compLen);
			} catch (ArrayIndexOutOfBoundsException e) {
				messages.add("Error when decompressing");
				return;
			}
			if (0 != Checksum.calc(pdu.getBytes(), pdu.length())) {
				messages.add("Checksum mismatch when decompressing");
				return;
			} else {
				//				System.out.println("Checksum ok");
			}
			GZIPInputStream unzipper;
			try {
				unzipper = new GZIPInputStream(new ByteArrayInputStream(compMsg));
				//Sets the size to 4 times the compressed size to hopefully have enough space
				byte[]uncompressed = new byte[msg.length * 4];
				unzipper.read(uncompressed);
				msg = uncompressed;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Date time = new Date(timeStamp*1000L);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeString = sdf.format(time);
		messages.add("[" + timeString + "] " + new String(nick,"UTF-8") + ": " + new String(msg, "UTF-8"));
	}

	public void getUserData(String username) throws IOException {
		lastSearch = username;
		byte[] cmd = Commands.whoIs(username);
		send(cmd);
	}

	/**
	 * Gets first message from message queue.
	 * @return a string containing the first message or null if the queue is empty
	 */
	public String getFirstMessage() {
		return messages.poll();
	}

	/**
	 * Gets the list of users currently connected to the server
	 * @return a string vector with user names
	 */
	public Vector<String> getNicks() {
		return nickList;
	}

	/**
	 * Checks if the user list has changed.
	 * @return true if it has changed
	 */
	public boolean nicksChanged() {
		return nicksChanged;
	}

	/**
	 * Used when the gui has responded to a nick change
	 * to wait for new changes.
	 */
	public void resetChange() {
		nicksChanged = false;
	}

	/**
	 * Gets the list of servers gotten from the name server
	 * @return
	 */
	public ArrayList<ServerID> getServers(){
		return servers;
	}

	public boolean getEncryption() {
		return encryption;
	}

	public void setEncryption(boolean enabled) {
		encryption = enabled;
	}

	public boolean getCompression() {
		return compression;
	}

	public void setCompression(boolean enabled) {
		compression = enabled;
	}

	public String getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(String s) {
		encryptionKey = s;
	}

	public String getNSaddr() {
		return nameServerAddress;
	}

	public int getNSport() {
		return nameServerPort;
	}

	public void setNSaddr(String addr) {
		nameServerAddress = addr;
	}

	public void setNSport(int port) {
		nameServerPort= port;
	}

}
