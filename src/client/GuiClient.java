package client;

import java.io.IOException;

/**
 * Class to start a client with a gui.
 *
 */
public class GuiClient {
	public static void main(String[] args) throws IOException {
		//The client uses its own thread to listen for incoming tcp from the server.
		ClientProgram program = new ClientProgram();

		GUI gui = new GUI(program);
		while (true) {
			String message = null;
			//New messages are added to a queue which is checked regularly
			if ((message = program.getFirstMessage()) != null) {
				gui.addMessage(message);

				//Responds to notifications about changes in the nick list.
			} else if (program.nicksChanged()) {
				gui.updateNicks(program.getNicks());
				program.resetChange();
			} else {
				//If nothing has happened, wait for 0.1 sec to avoid using
				//unnecessary processing power.
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// Thread error thing.
				}
			}
		}
	}
}
