package client;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;


/**
 * Pop-up dialog to change program settings such as encryption,
 * compression and name server.
 */
public class SettingsDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JCheckBox encryption;
	private JCheckBox compression;
	private JTextField encryptionKey;
	private JTextField NSaddr;
	private JTextField NSport;
	private ClientProgram program;

	public SettingsDialog(JFrame owner, ClientProgram program) {
		super(owner, "Settings");
		this.program = program;
		setLocationRelativeTo(owner);
		initalizeComponents();
		setupLayout();
		setVisible(true);
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	@Override
	public void dispose() {
		program.setEncryptionKey(encryptionKey.getText());
		program.setNSaddr(NSaddr.getText());
		try {
			program.setNSport(Integer.parseInt(NSport.getText()));
		} catch (Exception e) {
			program.setNSport(0);
		}

		super.dispose();
	}

	public void initalizeComponents() {
		encryption = new JCheckBox("Encryption", program.getEncryption());
		compression = new JCheckBox("Compression", program.getCompression());
		encryptionKey = new JTextField(program.getEncryptionKey(), 10);
		NSaddr = new JTextField(program.getNSaddr(), 10);
		NSport = new JTextField(new Integer(program.getNSport()).toString(), 10);
		encryption.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				boolean enabled = (arg0.getStateChange() == ItemEvent.SELECTED);
				program.setEncryption(enabled);
			}
		});
		compression.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				boolean enabled = (arg0.getStateChange() == ItemEvent.SELECTED);
				program.setCompression(enabled);
			}
		});
	}

	public void setupLayout() {
		JPanel container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		JPanel checkboxes = new JPanel();
		checkboxes.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		add(container);
		checkboxes.add(compression);
		checkboxes.add(encryption);
		container.add(checkboxes);
		JPanel textPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10,5));
		textPanel.add(new JLabel("Encryption key"));
		textPanel.add(encryptionKey);
		JPanel textPanel2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10,5));
		textPanel2.add(new JLabel("Name server Address"));
		textPanel2.add(NSaddr);
		JPanel textPanel3 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10,5));
		textPanel3.add(new JLabel("Name server Port"));
		textPanel3.add(NSport);
		container.add(textPanel);
		container.add(textPanel2);
		container.add(textPanel3);
	}
}
