package client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Scanner;

public class OverloadTest {

	/**
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		for (int i = 0; i < 200; i++) {
			ClientProgram cp = new ClientProgram();
			cp.connect(new InetSocketAddress("localhost",8080));
			cp.register();
			Thread.sleep(100);
		}
		new Scanner(System.in).nextLine();
		System.exit(0);
	}

}
