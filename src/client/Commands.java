package client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPOutputStream;

import common.Checksum;
import common.Crypt;
import common.MsgTypes;
import common.OpCodes;
import common.PDU;

/**
 * Collection of static methods used to generate byte arrays
 * according to the chat protocol.
 */
public class Commands {

	/**
	 * Makes a byte array for requesting server list from the
	 * name server according to protocol.
	 * The OP consists of 1 byte for the Opcode and 3 padded bytes.
	 * @return
	 */
	public static byte[] getList() {

		//4 bytes of header data
		PDU pdu = new PDU(4);

		pdu.setByte(0, (byte)OpCodes.GETLIST);
		pdu.setByte(1, (byte) 0);
		pdu.setShort(2, (short)0);
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array to act as a connection
	 * request to a server.
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] register(String nick) throws UnsupportedEncodingException {
		byte[] nickBytes = nick.getBytes("UTF-8");
		//4 bytes of header data
		PDU pdu = new PDU(4+nickBytes.length+calcPadding(nickBytes.length));

		pdu.setByte(0, (byte)OpCodes.JOIN);
		pdu.setByte(1, (byte) nickBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setSubrange(4,nickBytes);
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array that is sent to the server by a client. The server
	 * will then inform all clients that a client has left the server.
	 * The OP consists of 1 byte for the Opcode and 3 padded bytes.
	 * @return
	 */
	public static byte[] quit() {

		//4 bytes of header data
		PDU pdu = new PDU(4);

		pdu.setByte(0, (byte)OpCodes.QUIT);
		pdu.setByte(1, (byte)0);
		pdu.setShort(2, (short)0);
		return pdu.getBytes();
	}

	/**
	 * Makes the byte array to send a text message to the chat server
	 * @param message the text to be sent
	 * @param nick the nickname of the sender
	 * @param encryption marks if encryption should be used
	 * @param compression marks if compression should be used
	 * @return a byte array containing the command
	 */
	public static byte[] sendMessage(String message, String nick, boolean encryption, boolean compression, String encryptionKey) {

		byte[] msgBytes = null;
		PDU pdu = null;
		byte type;

		try {
			msgBytes = message.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		if (encryption && compression) {
			type = (byte)MsgTypes.COMPCRYPT;
			msgBytes = addCompression(msgBytes);
			msgBytes = addEncryption(msgBytes, encryptionKey);
		} else if (encryption) {
			type = (byte)MsgTypes.CRYPT;
			msgBytes = addEncryption(msgBytes, encryptionKey);
		} else if (compression) {
			type = (byte)MsgTypes.COMP;
			//Compresses message and adds compression header
			msgBytes = addCompression(msgBytes);
		} else {
			type = (byte)MsgTypes.TEXT;
		}

		//Makes sure that the lengths are divisible by 4
		int msgPad = calcPadding(msgBytes.length);
		pdu = new PDU(12+msgBytes.length + msgPad);

		pdu.setByte(0, (byte)OpCodes.MESSAGE);
		pdu.setByte(1, type);

		pdu.setByte(2, (byte)0); //Nick length

		pdu.setShort(4, (short)msgBytes.length);
		pdu.setShort(6, (short)0);

		pdu.setShort(8,(short)0); //Timestamp is added by the server
		pdu.setShort(10,(short)0);

		pdu.setSubrange(12,msgBytes);

		pdu.setByte(3,(byte)Checksum.calc(pdu.getBytes(), pdu.length())); //Checksum

		return pdu.getBytes();
	}

	private static byte[] addEncryption(byte[] msgBytes, String encryptionKey) {

		byte[] key = null;
		try {
			key = encryptionKey.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// Arbitrary missing file exception
		}
		int unencryptedLen = msgBytes.length;

		if (encryptionKey.length() != 0)
			Crypt.encrypt(msgBytes, msgBytes.length, key, key.length);

		PDU pdu = new PDU(8 + msgBytes.length);

		pdu.setByte(0, (byte)0);
		pdu.setShort(2, (short)msgBytes.length); //encrypted length
		pdu.setShort(4, (short)unencryptedLen); //unencrypted length
		pdu.setShort(6, (short)0); //padding
		pdu.setSubrange(8, msgBytes);

		pdu.setByte(1, (byte)Checksum.calc(pdu.getBytes(), pdu.length())); //checksum

		return pdu.getBytes();
	}

	/**
	 * Compresses text message and adds a header
	 * @param msgBytes the text message as a byte array
	 * @return a compressed version of the message with a header at the start
	 */
	private static byte[] addCompression(byte[] msgBytes) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		GZIPOutputStream zipper = null;
		try {
			zipper = new GZIPOutputStream(output);
			zipper.write(msgBytes);
			zipper.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] compressed = output.toByteArray();

		PDU pdu = new PDU(8+compressed.length);
		pdu.setByte(0, (byte)0);
		pdu.setShort(2, (short)compressed.length);
		pdu.setShort(4, (short)msgBytes.length);
		pdu.setShort(6, (short)0);
		pdu.setSubrange(8, compressed);

		pdu.setByte(1, (byte)Checksum.calc(pdu.getBytes(), pdu.length())); //checksum
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array for changing user nickname according to protocol.
	 * The OP consists of 1 byte for Opcode, 1 byte for nick length, 2 bytes
	 * of padding then n bytes for the actual nickname, where n is the length
	 * of the nickname.
	 * @param nick
	 * @return byte array containing the command
	 */
	public static byte[] changeNick(String nick) {

		byte[] nickBytes = null;
		try {
			nickBytes = nick.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// Arbitrary missing library exception.
		}

		//4 bytes of header data
		PDU pdu = new PDU(4+nickBytes.length+calcPadding(nickBytes.length));

		pdu.setByte(0, (byte)OpCodes.CHNICK);
		pdu.setByte(1, (byte)nickBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setSubrange(4, nickBytes);
		return pdu.getBytes();
	}

	/**
	 * Makes the byte array to change topic
	 * @param topic the topic to change to
	 * @return byte array containing the command
	 */
	public static byte[] changeTopic(String topic) {

		byte[] topicBytes = null;
		try {
			topicBytes = topic.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		//4 bytes of header data
		PDU pdu = new PDU(4+topicBytes.length+calcPadding(topicBytes.length));

		pdu.setByte(0, (byte)OpCodes.CHTOPIC);
		pdu.setByte(1, (byte)topicBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setSubrange(4, topicBytes);
		return pdu.getBytes();
	}

	public static byte[] whoIs(String username){
		byte[]  userBytes = null;
		try {
			userBytes =  username.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		PDU pdu = new PDU(4+userBytes.length+calcPadding(userBytes.length));

		pdu.setByte(0,(byte) OpCodes.WHOIS);
		pdu.setByte(1, (byte)userBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setSubrange(4, userBytes);
		return pdu.getBytes();
	}

	/**
	 * Calculates how much that needs to be added to make a number
	 * divisible by 4
	 * @param size the number to be examined
	 * @return an int from 0 to 3
	 */
	public static int calcPadding(int size) {
		return (4 - (size % 4))%4;
	}

	public static short getUnsigned(byte b) {
		return (short) (b & 0xFF);
	}
	public static int getUnsigned(short b) {
		return (int) (b & 0xFFFF);
	}
	public static long getUnsigned(int b) {
		return (long) (b & 0xFFFFFFFF);
	}
}


