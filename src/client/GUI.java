package client;

import java.util.Vector;

import javax.swing.*;

/**
 * GUI.java
 *
 * The GUI class deals primarily with the creation and initiation of GUI components.
 */

@SuppressWarnings("serial")
public class GUI extends JFrame {

	// GUI components
	private JButton sendButton;
	private JList<String> userList;
	private JMenu fileMenu;
	private JMenuBar menuBar;
	private JMenuItem connect;
	private JMenuItem quit;
	public JPanel displayPanel;
	private JPanel inputPanel;
	private JPanel userPanel;
	private JScrollPane displayScroll;
	private JScrollPane inputScroll;
	private JScrollPane userScroll;
	private JTextArea displayArea;
	private JTextField inputArea;
	private JMenuItem disconnect;
	private JMenuItem changeNick;
	public ClientProgram client;
	private JMenuItem changeTopic;
	private JMenuItem settings;
	private JPopupMenu popUp = new JPopupMenu();
	private JMenuItem whoIs = new JMenuItem("Who is?");


	// Creation of aforementioned components.

	public GUI(ClientProgram client) {
		this.client = client;
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				displayPanel = new JPanel();
				displayScroll = new JScrollPane();
				displayArea = new JTextArea();

				inputPanel = new JPanel();
				inputScroll = new JScrollPane();
				inputArea = new JTextField();

				userScroll = new JScrollPane();
				userList = new JList<String>();
				userPanel = new JPanel();
				popUp.add(whoIs);
				userList.setComponentPopupMenu(popUp);

				sendButton = new JButton();
				sendButton.setText("Send");
				menuBar = new JMenuBar();
				fileMenu = new JMenu();
				connect = new JMenuItem();
				disconnect = new JMenuItem();
				changeNick = new JMenuItem();
				changeTopic = new JMenuItem();
				settings = new JMenuItem();
				quit = new JMenuItem();

				displayArea.setEditable(false);
				displayArea.setColumns(20);
				displayArea.setRows(5);

				displayScroll.setViewportView(displayArea);
				userScroll.setViewportView(userList);
				inputScroll.setViewportView(inputArea);

				userList.setModel(new javax.swing.AbstractListModel<String>() {
					String[] strings = { };
					public int getSize() {
						return strings.length;
					}
					public String getElementAt(int i){
						return strings[i];
					}
				}
						);

				inputArea.setColumns(20);

				initiateLayouts();
				initiateMenus();

				setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
				setTitle("Chat Client");
				setResizable(false);
				pack();
				validate();
				setVisible(true);

			}
		});
	}
	/**
	 * updateNicks is used to update the list of clients currently on the server in the
	 * GUI's user list.
	 * @param nicks - A vector of client names.
	 * @return
	 */
	public void updateNicks(final Vector<String> nicks) {
		userList.setListData(nicks);
	}

	/**
	 * Internal method used to assign menu actions and coordinates
	 * how the menu handler works with the GUI.
	 * @param
	 * @return
	 */
	private void initiateMenus() {

		fileMenu.setText("File");
		connect.setText("Connect");
		connect.addActionListener(new MenuHandler(1, this));
		fileMenu.add(connect);

		disconnect.setText("Disconnect");
		disconnect.addActionListener(new MenuHandler(2, this));
		fileMenu.add(disconnect);

		fileMenu.addSeparator();

		changeNick.setText("Change Nickname");
		changeNick.addActionListener(new MenuHandler(4, this));
		fileMenu.add(changeNick);

		changeTopic.setText("Change Topic");
		changeTopic.addActionListener(new MenuHandler(5, this));
		fileMenu.add(changeTopic);

		settings.setText("Settings");
		settings.addActionListener(new MenuHandler(6, this));
		fileMenu.add(settings);

		fileMenu.addSeparator();

		quit.setText("Quit");
		quit.addActionListener(new MenuHandler(0, this));
		fileMenu.add(quit);
		menuBar.add(fileMenu);

		sendButton.addActionListener(new MenuHandler(3, this));
		inputArea.addActionListener(new MenuHandler(3, this));

		whoIs.addActionListener(new MenuHandler(7, this));

		setJMenuBar(menuBar);

	}

	/**
	 * Internal method used to initiate GUI layouts.
	 * @param
	 * @return
	 */
	private void initiateLayouts(){

		org.jdesktop.layout.GroupLayout displayLayout = new org.jdesktop.layout.GroupLayout(displayPanel);
		displayPanel.setLayout(displayLayout);
		displayLayout.setHorizontalGroup(
				displayLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(displayLayout.createSequentialGroup()
						.add(displayScroll, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 700, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
						.add(0, 0, Short.MAX_VALUE))
				);

		displayLayout.setVerticalGroup(
				displayLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(displayLayout.createSequentialGroup()
						.add(displayScroll, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 336, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
						.add(0, 0, Short.MAX_VALUE))
				);

		org.jdesktop.layout.GroupLayout inputLayout = new org.jdesktop.layout.GroupLayout(inputPanel);
		inputPanel.setLayout(inputLayout);
		inputLayout.setHorizontalGroup(
				inputLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(org.jdesktop.layout.GroupLayout.TRAILING, userScroll, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
				);

		inputLayout.setVerticalGroup(
				inputLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(userScroll)
				);

		org.jdesktop.layout.GroupLayout userLayout = new org.jdesktop.layout.GroupLayout(userPanel);
		userPanel.setLayout(userLayout);
		userLayout.setHorizontalGroup(
				userLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(userLayout.createSequentialGroup()
						.add(inputScroll)
						.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(sendButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 83, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
				);

		userLayout.setVerticalGroup(
				userLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(org.jdesktop.layout.GroupLayout.TRAILING, userLayout.createSequentialGroup()
						.add(0, 0, Short.MAX_VALUE)
						.add(userLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
								.add(sendButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
								.add(inputScroll, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
				);

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(layout.createSequentialGroup()
						.addContainerGap()
						.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
								.add(displayPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.add(userPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.add(18, 18, 18)
								.add(inputPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addContainerGap())
				);

		layout.setVerticalGroup(
				layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(layout.createSequentialGroup()
						.add(11, 11, 11)
						.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
								.add(inputPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.add(layout.createSequentialGroup()
										.add(displayPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
										.add(18, 18, 18)
										.add(userPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
										.addContainerGap())
				);
	}

	/**
	 * Fetches text from chat field.
	 * @param
	 * @return message
	 */
	public String getMessage(){
		String message = inputArea.getText();
		inputArea.setText("");
		return message;
	}

	/**
	 * Adds a text message to the chat window.
	 * @param message
	 * @return
	 */
	public void addMessage(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				displayArea.append(message+"\n\n");
				displayArea.setCaretPosition(displayArea.getDocument().getLength());
			}
		});
	}

	public String getUser(){
		return userList.getSelectedValue();
	}

}
