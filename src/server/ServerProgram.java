package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import common.Checksum;
import common.MsgTypes;
import common.OpCodes;
import common.PDU;
import common.UDPConnection;

/**
 * ServerProgram.java
 *
 * ServerProgram contains all of the server-side intelligence and deals with all data retrieval
 * as well as calling Commands to create byte arrays that will be sent to either the client of name
 * server. It keeps a list of connected users as well as maintaining a connection to the namerserver.
 */

public class ServerProgram {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String nsAddr = "itchy.cs.umu.se";
		int nsPort = 1337;
		if (args.length == 2) {
			nsAddr = args[0];
			try {
				int num = Integer.parseInt(args[1]);
				nsPort = num;
			} catch (NumberFormatException e) {
				System.out.println("Port needs to be an integer");
				System.exit(1);
			}

		}

		new ServerProgram(nsAddr, nsPort);
	}

	private Selector selector;
	private int port = 8080;
	private String topic = "PRAISE THE SUN";
	private int id = -1;
	private UDPConnection nameServer;
	private String nameServerAddress = "itchy.cs.umu.se";
	private int nameServerPort = 1337;
	private ArrayList<User> users;

	public ServerProgram(String nsAddr, int nsPort) throws IOException {
		nameServerAddress = nsAddr;
		nameServerPort = nsPort;

		nameServer = new UDPConnection(nameServerAddress, nameServerPort);
		users = new ArrayList<User>();
		selector = Selector.open();
		registerAtNS();
		new Thread(aliveLoop()).start();
		new Thread(listenForNewConnections()).start();
		listenForInput().run();
	}


	/**
	 * Gets a Runnable which can be started in a separate thread.
	 * It loops and listens for input on any of its connections.
	 * A selector is used to get which clients that want to send data.
	 */
	public Runnable listenForInput() {
		return new Runnable() {
			@Override
			public void run() {
				while (true) {
					int count = 0;
					try {
						//Sees if any client wants to write. Times out after 2 secs.
						count = selector.select(500);
					} catch (IOException e) {
						e.printStackTrace();
					}
					//Start again if nothing happened
					if (count == 0) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						continue;
					}
					//Gets all clients that want to write. They are represented
					//as 'SelectionKeys' which is a kind of wrapper to the socket.
					Set<SelectionKey> keySet = selector.selectedKeys();
					Iterator<SelectionKey> selected = keySet.iterator();
					//Loop over those clients.
					while (selected.hasNext()) {
						SelectionKey key = selected.next();
						selected.remove(); //Needed to not read the same key twice
						if (key.isReadable() && key.isValid()) {
							//Gets the User instance attached to the key
							User sender = (User) key.attachment();
							SocketChannel channel = sender.getChannel();
							int bufsize = 50;
							ByteBuffer buffer = ByteBuffer.allocate(bufsize);
							try {
								int bytesRead = 0;
								while ((bytesRead = channel.read(buffer)) > 0) {
									buffer.flip();
									if (sender.isWriting()) {
										storeIncompleteCommand(sender, buffer);
										ByteBuffer totalCmd = ByteBuffer.wrap(sender.getStartedCommand());
										if (isEntireCommand(totalCmd)) {
											buffer = totalCmd;
											sender.reset();
										} else {
											continue;
										}
									} else if (isEntireCommand(buffer)) {
									} else {
										storeIncompleteCommand(sender, buffer);
										continue;
									}
									buffer.mark();
									//Gets the first byte which decides what command is being sent.
									byte op = buffer.get();

									//Checks first if the user is registered. All data from
									//unregistered users is discarded unless it is a register command.
									if (!sender.isRegistered()) {
										if (op == OpCodes.JOIN) {
											registerUser(sender,buffer);
										} else {
											System.out.println("Unregistered user trying to write");
										}
									} else {
										switch (op) {
										case OpCodes.CHNICK:
											changeNick(sender, buffer);
											break;
										case OpCodes.MESSAGE:
											if (!message(sender, buffer)) {
												removeUser(sender, "Checksum mismatch");
											}
											break;
										case OpCodes.QUIT:
											removeUser(sender);
											break;
										case OpCodes.CHTOPIC:
											changeTopic(sender, buffer);
											break;
										case OpCodes.WHOIS:
											whoIs(sender, buffer);
											break;
										case OpCodes.JOIN:
											break;
										case 0: //Padding
											break;
										default:
											System.out.println("Unknown OPCode: " + op);
											removeUser(sender, "Invalid command");
											break;
										}
									}

									buffer.clear();
									buffer = ByteBuffer.allocate(bufsize);
								}
								//If bytesRead is -1 that means the socket is broken and the client has disconnected.
								//Disconnects need to be handled in several ways depending on if the client disconnected
								//voluntarily or if the program crashed etc.
								if (bytesRead == -1) {
									removeUser(sender);
								}
							} catch (BufferUnderflowException e) {
								System.out.println("Command length shorter than expected");
								removeUser(sender, "Command length mismatch");
							} catch (ClosedChannelException e) {
								System.out.println("Channel closed");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		};
	}

	private void removeUser(User user) {
		users.remove(user);
		sendToAll(Commands.userLeft(user));
		if (user.getChannel().isConnected())
			try {
				user.getChannel().close();
			} catch (IOException e) {
			}
	}

	private void removeUser(User user, String message) {
		sendToAll(systemMessage(user.getNickname() + " has disobeyed the rules and is no longer with us.\nReason: " + message));
		removeUser(user);
	}

	private void storeIncompleteCommand(User user, ByteBuffer buffer) {
		buffer.mark();
		byte[] inputArray = new byte[buffer.limit()];
		buffer.get(inputArray);
		buffer.reset();
		user.commandStart(inputArray);
	}

	private boolean isEntireCommand(ByteBuffer input) {
		input.mark();
		byte[] inputArray = new byte[input.limit()];
		input.get(inputArray);
		int length = 0;
		PDU pdu = new PDU(inputArray, inputArray.length);
		try {
			short op = pdu.getByte(0);

			switch (op) {
			case OpCodes.JOIN:
				length = pdu.getByte(1) + 4;
				break;
			case OpCodes.CHNICK:
				length = pdu.getByte(1) + 4;
				break;
			case OpCodes.MESSAGE:
				length = pdu.getShort(4) + 12;
				break;
			case OpCodes.CHTOPIC:
				length = pdu.getByte(1)+4;
				break;
			case OpCodes.WHOIS:
				length = pdu.getByte(1)+4;
				break;
			case OpCodes.QUIT:
				length = 4;
				break;
			default:
				break;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		} finally {
			input.reset();
		}
		return length <= pdu.length();
	}

	public void whoIs(User sender, ByteBuffer buffer) {
		short nickLen = Commands.getUnsigned(buffer.get());
		//Throws away padding
		buffer.getShort();

		byte[] nickName = new byte[nickLen];
		buffer.get(nickName);
		String decoded = null;
		try {
			decoded = new String(nickName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.err.println("Missing library UTF-8.\n");
		}

		byte[] cmd = Commands.findUser(users,decoded);
		sender.send(cmd);
	}

	/**
	 * Makes a runnable to listen for new TCP connections. Used with a thread to run simultaneously as the input listening loop.
	 * When a new connection is established it is registered with the selector so that all connections can be handled by it.
	 * @return
	 */
	public Runnable listenForNewConnections() {
		return new Runnable() {

			@Override
			public void run() {
				ServerSocketChannel serverChannel = null;
				try {
					//Starts a welcome socket at port 8080
					serverChannel = ServerSocketChannel.open();
					serverChannel.bind(new InetSocketAddress(8080));
					while (true) {
						SocketChannel newConnection = serverChannel.accept();
						System.out.println("New connection accepted");
						//Makes the channel nonblocking to avoid deadlocks.
						newConnection.configureBlocking(false);
						User user = new User(newConnection);
						selector.wakeup(); //Stops selector if it is currently waiting
						//Registers connection with selector so that it can respond to
						//input from the socket channel.
						newConnection.register(selector, SelectionKey.OP_READ, user);
					}
				} catch (IOException e) {
					System.err.println("Problem with connection\n");
				}
			}
		};
	}

	/**
	 * Makes a runnable responsible for name server connection. Tries to register with name server
	 * and when that succeeds sends regular alive messages. All commands are resent if they don't get
	 * a proper response.
	 */
	public Runnable aliveLoop() {
		return new Runnable() {
			@Override
			public void run() {
				while (true) {
					byte[] cmd = Commands.alive(users.size(), id);
					try {
						byte[] response = nameServer.send(cmd);
						if (Commands.isAck(response, id)) {
							System.out.println("Alive OK");
							//The name server requires alive messages once every 30 seconds
							//so the thread can sleep after a message is acknowledged.
							Thread.sleep(20000);
						} else if (Commands.isNotReg(response)) {
							//Tries to re-register if the name server
							//doesn't recognize the chat server.
							registerAtNS();
						} else {
							System.out.println("No response from name server");
						}
					} catch (IOException e) {
						System.err.println("Problem with Name Server communication\n");
					} catch (InterruptedException e) {
						System.err.println("Problem with alive thread.\n");
					}
				}
			}
		};
	}

	/**
	 * Called when a new client has connected.
	 * @param channel The channel of the calling user
	 * @param buffer The buffer containing the data. The first byte
	 *               which contains the opcode is assumed to already
	 *               be read when calling.
	 */
	private void registerUser(User client, ByteBuffer buffer) {
		short nickLen = Commands.getUnsigned(buffer.get());
		//Throws away padding
		buffer.getShort();

		byte[] nickName = new byte[nickLen];
		buffer.get(nickName);
		String decoded = null;
		try {
			decoded = new String(nickName, "UTF-8");
		} catch (UnsupportedEncodingException e) {

		}
		decoded = getAlternativeNick(decoded);

		client.setNickname(decoded);
		client.register();
		users.add(client);
		try {
			sendToAll(Commands.userJoined(client));
		} catch (UnsupportedEncodingException e) {
			System.out.println("Unable to inform clients of new client.");
		}

		byte[] cmd = Commands.nickList(users,client);
		client.send(cmd);
		client.send(systemMessage("Welcome to server!"));

	}

	private byte[] systemMessage(String msg) {

		byte[] msgBytes;
		byte[] cmd = null;
		try {
			msgBytes = msg.getBytes("UTF-8");
			cmd = Commands.message("", msgBytes, (byte)MsgTypes.TEXT, (byte)0);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return cmd;
	}

	/**
	 * Called when a change nick command is detected.
	 * @param user The channel of the calling user
	 * @param buffer The buffer containing the data. The first byte
	 *               which contains the opcode is assumed to already
	 *               be read when calling.
	 */
	private void changeNick(User sender, ByteBuffer buffer) {
		short nickLen = Commands.getUnsigned(buffer.get());
		//Throws away padding
		buffer.getShort();

		byte[] nickName = new byte[nickLen];
		buffer.get(nickName);
		String decoded = null;
		try {
			decoded = new String(nickName, "UTF-8");
		} catch (UnsupportedEncodingException e) {

		}
		decoded = getAlternativeNick(decoded);

		byte[] cmd = null;
		try {
			cmd = Commands.newNick(sender.getNickname(), decoded);
		} catch (UnsupportedEncodingException e) {

		}
		sendToAll(cmd);
		sender.setNickname(decoded);
		System.out.println("New nick: " + decoded);
	}

	/**
	 * Used to make sure all nicknames are unique. Loops the list of connected
	 * users and checks if the nickname is already taken and if so adds a
	 * number at the end. The comparison is case insensitive.
	 * @param nick the nickname to examine
	 * @return If the nickname is already unique
	 * it is returned as is. Otherwise it is returned with a number at the end.
	 */
	private String getAlternativeNick(String nick) {
		int i = 0;
		boolean keepLooking = true;
		//Repeats until a unique name is found and increases the number at the end each time
		while (keepLooking) {
			boolean taken = false;
			for (int j = 0; j < users.size(); j++) {
				//Only add number if it is not 0
				String s = nick + (i > 0 ? i : "");
				if (users.get(j).getNickname().equalsIgnoreCase(s)) {
					i++;
					taken = true;
					break;
				}
			}
			if (!taken) {
				keepLooking = false;
			}
		}
		//Only add number if it is not 0
		return nick + (i > 0 ? i : "");
	}

	/**
	 * Responds to change topic command. Notifies all users and the name server
	 * of the change.
	 * @param sender the user that changed the topic
	 * @param buffer the buffer containing the command
	 * @throws IOException
	 */
	private void changeTopic(User sender, ByteBuffer buffer) throws IOException {

		short tpcLen = Commands.getUnsigned(buffer.get());
		buffer.getShort();
		byte[] topicBytes = new byte[tpcLen];
		buffer.get(topicBytes);

		byte[] cmd = null;
		cmd = Commands.newTopic(topicBytes,id);

		byte[] response = nameServer.send(cmd);
		if (response != null) {
			PDU pdu = new PDU(response, 4);
			short op = pdu.getByte(0);
			if (op == OpCodes.ACK) {
				System.out.println("Topic changed!\n");
				sendToAll(Commands.informTopic(topicBytes,id));
			} else {
				System.out.println("Unrecognized response from name server");
			}
		} else {
			System.out.println("No response from name server");
		}
	}

	/**
	 * Responds to text message from a user. The message is just passed along for the most
	 * part but the senders nickname and a timestamp is added.
	 * @param sender
	 * @param buffer
	 */
	private boolean message(User sender, ByteBuffer buffer) {


		byte msgType = buffer.get();
		buffer.get();
		byte checkSum = buffer.get();
		int msgLen = Commands.getUnsigned(buffer.getShort());
		buffer.getShort(); //padding
		buffer.getInt();

		byte[] msg = new byte[msgLen];
		buffer.get(msg);

		buffer.reset();
		byte[] entireCmd = new byte[12+msgLen];
		buffer.get(entireCmd);

		if (0 != Checksum.calc(entireCmd, entireCmd.length)) {
			System.out.println("Checksum does not match");
			return false;
		} else {
			try {
				byte[] cmd = Commands.message(sender.getNickname(), msg, msgType, checkSum);
				System.out.println("Message received: " + new String(msg, "UTF-8"));
				sendToAll(cmd);
			} catch (UnsupportedEncodingException e) {
			}
		}
		return true;

	}

	/**
	 * Sends a command to all registered users.
	 * @param buffer a byte array containing the command
	 */
	private void sendToAll(byte[] buffer) {
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (user.isRegistered()) {
				user.send(buffer);
			}
		}
	}

	/**
	 * Registers the chat server with a name server.
	 * @return True on acknowledged register, false otherwise.
	 * @throws IOException
	 */
	private boolean registerAtNS() throws IOException {
		//Tries to get global ip from external website to get past router problems.
		String ip = findIP();
		byte[] ipBytes;
		if (ip != null) {
			ipBytes = InetAddress.getByName(ip).getAddress();
		} else {
			ipBytes = InetAddress.getLocalHost().getAddress();
		}
		byte[] regCmd = Commands.register(topic, ipBytes, port);
		byte[] response = nameServer.send(regCmd);
		if (response != null) {
			PDU pdu = new PDU(response, 4);
			short op = pdu.getByte(0);
			if (op == OpCodes.ACK) {
				id = pdu.getShort(2);
				System.out.println("Register OK, id = " + id);
				return true;
			} else {
				System.out.println("Unrecognized response from name server");
			}
		} else {
			System.out.println("No response from name server");
		}
		return false;
	}

	/**
	 * Uses a website to get the current machine's global ip. This is
	 * necessary in some cases where the network interface
	 * only has access to a local address.
	 * @return The global ip as a string.
	 * @throws IOException
	 */
	private String findIP() throws IOException {
		URL url = null;
		try {
			url = new URL("http://checkip.amazonaws.com");
		} catch (MalformedURLException e) {

		}
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		String ipAddress = new String();
		ipAddress = (in.readLine()).trim();

		//Returns null on empty string.
		return ipAddress.length() != 0 ? ipAddress : null;
	}

}
