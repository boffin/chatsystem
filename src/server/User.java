package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * User datatype. Contains info such as time joined, nickname, allocated channel,
 * messages being sent, the users address and whether the user is registered or not.
 * Methods are self explanatory.
 */

public class User {

	private final int TIMEOUT = 3000;

	private SocketChannel channel;
	private String nickname;
	private boolean registered;
	private int timeJoined;
	private InetSocketAddress ipAddress;
	private Queue<byte[]> messages;
	private boolean isWriting;
	private long commandStartTime;
	private byte[] startedCommand;

	public User(final SocketChannel channel) {
		this.channel = channel;
		nickname = "Anonymous";
		registered = false;
		isWriting = false;
		commandStartTime = 0;
		messages = new ConcurrentLinkedQueue<byte[]>();
		timeJoined = Commands.makeTimestamp();
		try {
			ipAddress = (InetSocketAddress) channel.getRemoteAddress();
		} catch (IOException e) {
			e.printStackTrace();
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					byte[] cmd = messages.poll();
					if (cmd != null) {
						ByteBuffer bb = ByteBuffer.wrap(cmd);
						while (bb.hasRemaining()) {
							try {
								channel.write(bb);
							} catch (IOException e) {
								// TODO ERROR HERE
								System.err.println("User unable to write.");
								break;
							}
						}
					} else {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
		}).start();
	}


	public boolean isWriting() {
		long now = System.currentTimeMillis();
		long diff = now - commandStartTime;
		if (diff > TIMEOUT) {
			isWriting = false;
			startedCommand = null;
		}
		return isWriting;
	}

	/**
	 * IN PROGRESS
	 * @param cmd
	 */
	public void commandStart(byte[] cmd) {
		if (isWriting) {

			//append cmd to startedcommand
			byte[] newCmd = new byte[startedCommand.length + cmd.length];
			//Copy old command to new array
			System.arraycopy(startedCommand,0,newCmd,0,startedCommand.length);
			//copy new command right after the old
			System.arraycopy(cmd, 0, newCmd, startedCommand.length, cmd.length);
			cmd = newCmd;
		}
		startedCommand = cmd;
		commandStartTime = System.currentTimeMillis();
		isWriting = true;
	}

	public void reset() {
		isWriting = false;
		startedCommand = null;
		commandStartTime = 0;
	}

	public void addToCommand(byte[] cmd) {

	}

	public byte[] getStartedCommand() {
		return startedCommand;
	}

	public void send(byte[] cmd) {
		messages.add(cmd);
	}

	public void register() {
		registered = true;
	}
	public boolean isRegistered() {
		return registered;
	}

	public void setNickname(String nick) {
		nickname = nick;
	}
	public String getNickname() {
		return nickname;
	}

	public SocketChannel getChannel() {
		return channel;
	}

	public int getTimeJoined(){
		return timeJoined;
	}

	public byte[] getIpAddress(){
		return ipAddress.getAddress().getAddress();
	}
}
