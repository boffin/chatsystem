package server;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import common.Checksum;
import common.OpCodes;
import common.PDU;

/**
 * Collection of static methods used to generate byte arrays
 * according to the chat protocol.
 */
public class Commands {

	/**
	 * Makes a byte array to inform the nameserver that the server is still alive.
	 * The OP consists of 1 byte for Opcode, 1 byte for the number of clients and
	 * 2 bytes for the id allocated to the server by the nameserver.
	 * @param clients - number of clients , id - id allocated by nameserver.
	 * @return byte array containing the command
	 */
	public static byte[] alive(int clients, int id) {
		PDU pdu = new PDU(4);
		pdu.setByte(0, (byte)OpCodes.ALIVE);
		pdu.setByte(1, (byte)clients);
		pdu.setShort(2, (short)id);
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array to register at the nameserver.
	 * The OP consists of 1 byte for Opcode, 1 byte for the length of the topic,
	 * 2 bytes for the port the server is working on and finally bytes for the server
	 * address and the topic.
	 * @param topic - the servers topic, senderIP - the servers address , senderPort - the serversPort.
	 * @return byte array containing the command
	 */
	public static byte[] register(String topic, byte[] senderIP, int senderPort) {
		byte[] topicBytes = null;
		try {
			topicBytes = topic.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// Missing library exception.
			System.err.println("Missing library UTF-8.\n");
		}
		//Sets padding length so total length is dividable by 4
		int padding =  4 - (topicBytes.length % 4);
		//8 bytes of header data
		PDU pdu = new PDU(8+topicBytes.length + padding);

		pdu.setByte(0, (byte)OpCodes.REG);
		pdu.setByte(1, (byte)topicBytes.length);
		pdu.setShort(2, (short)senderPort);
		pdu.setSubrange(4, senderIP);
		pdu.setSubrange(8, topicBytes);
		return pdu.getBytes();
	}

	/**
	 * Appends a message sent by a client with a timestamp and a client allocation.
	 * @param senderNick - The client who sent the message, msg - the message,
	 * msgType - Defines the type of message.
	 * @return byte array containing the command
	 */
	public static byte[] message(String senderNick, byte[] msg, byte msgType, byte checksum) throws UnsupportedEncodingException {
		byte[] nickBytes = senderNick.getBytes("UTF-8");
		int nickLen = nickBytes.length;
		int msgLen = msg.length;
		int timeStamp = makeTimestamp();

		//12 bytes of header data
		PDU pdu = new PDU(12 + msgLen + nickLen + calcPadding(nickBytes.length) + calcPadding(msg.length));

		pdu.setByte(0, (byte) OpCodes.MESSAGE); //opcode
		pdu.setByte(1, (byte) msgType); //message type, not implemented yet
		pdu.setByte(2, (byte)nickLen); //nick length
		pdu.setShort(4, (short) msgLen); //message length
		pdu.setShort(6, (short)0); //padding
		pdu.setInt(8, timeStamp); //timestamp, uses unix time
		pdu.setSubrange(12, msg); //message text
		pdu.setSubrange(12+msgLen + calcPadding(msg.length), nickBytes); //nickname

		pdu.setByte(3, Checksum.calc(pdu.getBytes(), pdu.length())); //checksum
		return pdu.getBytes();
	}

	/**
	 * Checks to see if a message sent to the nameserver was acknowledged.
	 * @param response - array of bytes received from the nameserver,
	 * 	expectedId - The id of the server.
	 * @return True if acknowledged, otherwise false.
	 */
	public static boolean isAck(byte[] response, int expectedId) {
		if (response != null) {
			PDU pdu = new PDU(response, 4);
			short op = pdu.getByte(0);
			if (op == OpCodes.ACK) {
				int id = pdu.getShort(2);
				if (id == expectedId)
					return true;
			}
		}
		return false;
	}

	/**
	 * Checks to see if the nameserver returns a not registered response.
	 * @param response - array of bytes received from the nameserver.
	 * @return True if not registered, otherwise false.
	 */
	public static boolean isNotReg(byte[] response) {
		if (response != null) {
			byte op = response[0];
			if (op == OpCodes.NOTREG)
				return true;
		}
		return false;
	}

	/**
	 * Makes a byte array to deal with a change nickname request. The OP consists of 1 byte for Opcode,
	 * 1 byte for the length of the old nick, 1 byte for the length of the new nick,
	 * 1 byte of padding, 4 bytes for a timestamp, and then bytes for the old and new nicks.
	 * @param oldNick - Old nickname, newNick - New nickname
	 * @return byte array containing the command
	 */
	public static byte[] newNick(String oldNick, String newNick) throws UnsupportedEncodingException {
		byte[] oldBytes = oldNick.getBytes("UTF-8");
		byte[] newBytes = newNick.getBytes("UTF-8");
		int oldLength = oldBytes.length + calcPadding(oldBytes.length);
		int newLength = newBytes.length + calcPadding(newBytes.length);
		PDU pdu = new PDU(8 + oldLength + newLength);
		pdu.setByte(0, (byte)OpCodes.UCNICK);
		pdu.setByte(1, (byte)oldBytes.length);
		pdu.setByte(2, (byte)newBytes.length);
		pdu.setByte(3, (byte)0);
		pdu.setInt(4, makeTimestamp());
		pdu.setSubrange(8, oldBytes);
		pdu.setSubrange(8 + oldLength, newBytes);

		return pdu.getBytes();
	}

	/**
	 * Makes a byte array to deal with a topic change request to the nameserver.
	 * The OP consists of 1 byte for Opcode, 1 byte for the length of the topic,
	 * 2 bytes for the server id and lastly the bytes for the topic name.
	 * @param topicBytes - Byte of the topic, tpcLen - Length of the topic, id - server id.
	 * @return byte array containing the command
	 */
	public static byte[] newTopic(byte[] topicBytes, int id){
		PDU pdu = new PDU(4+topicBytes.length+calcPadding(topicBytes.length));

		pdu.setByte(0, (byte)OpCodes.CHTOPIC);
		pdu.setByte(1, (byte)topicBytes.length);
		pdu.setShort(2, (short)id);
		pdu.setSubrange(4, topicBytes);

		return pdu.getBytes();
	}

	/**
	 * Makes a byte array to deal with informing all clients of a change of topic. The OP consists of 1 byte for Opcode,
	 * 1 byte for the topic length, 2 bytes of padding and finally the bytes of the topic name.
	 * @param topicBytes - bytes of the topic , id - the servers id.
	 * @return byte array containing the command
	 */
	public static byte[] informTopic(byte[] topicBytes, int id) {

		//4 bytes of header data
		PDU pdu = new PDU(4+topicBytes.length+calcPadding(topicBytes.length));

		pdu.setByte(0, (byte)OpCodes.CHTOPIC);
		pdu.setByte(1, (byte)topicBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setSubrange(4, topicBytes);
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array to inform all clients that a user has joined. The OP consists of 1 byte for Opcode,
	 * 1 byte for the length of the new clients name, 2 bytes of padding, 4 bytes for the time the user joined,
	 * and finally the bytes for the new clients name.
	 * @param client - the client that joined.
	 * @return byte array containing the command
	 */
	public static byte[] userJoined(User client) throws UnsupportedEncodingException {
		byte[] nickBytes = client.getNickname().getBytes("UTF-8");
		//4 bytes of header data
		PDU pdu = new PDU(8+nickBytes.length+calcPadding(nickBytes.length));

		pdu.setByte(0, (byte)OpCodes.UJOIN);
		pdu.setByte(1, (byte) nickBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setInt(4,makeTimestamp());
		pdu.setSubrange(8,nickBytes);
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array to inform all clients that a user has left. The OP consists of 1 byte for Opcode,
	 * 1 byte for the length of the new clients name, 2 bytes of padding, 4 bytes for the time the user left,
	 * and finally the bytes for the new clients name.
	 * @param client - the client that left.
	 * @return byte array containing the command
	 */
	public static byte[] userLeft(User client) {
		byte[] nickBytes = null;
		try {
			nickBytes = client.getNickname().getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		//4 bytes of header data
		PDU pdu = new PDU(8+nickBytes.length);

		pdu.setByte(0, (byte)OpCodes.ULEAVE);
		pdu.setByte(1, (byte) nickBytes.length);
		pdu.setShort(2, (short)0);
		pdu.setInt(4,makeTimestamp());
		pdu.setSubrange(8,nickBytes);
		return pdu.getBytes();
	}

	/**
	 * Makes a byte array that contains all the clients connected to the server. The OP consists of 1 byte for Opcode,
	 * 1 byte for the number of users and will then continue to add usernames to the array.
	 * @param users - The users connected to the server.
	 * @return byte array containing the command
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] nickList(ArrayList<User> users, User client){
		int totalLength = 0;
		int headerLength = 4;
		int position = headerLength;
		PDU pdu = new PDU(headerLength);
		pdu.setByte(0, (byte)OpCodes.NICKS);
		pdu.setByte(1, (byte)users.size());

		byte[] op = null;
		try {
			op = client.getNickname().getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			System.err.println("Missing library UTF-8.\n");
		}
		totalLength += op.length+1;
		pdu.extendTo(headerLength + totalLength);
		pdu.setSubrange(position, op);
		position = headerLength + totalLength;

		for (int i = 0; i < users.size(); i++) {
			if (!users.get(i).getNickname().equals(client.getNickname())) {
				User u = users.get(i);
				byte[] nick = null;
				try {
					nick = u.getNickname().getBytes("UTF-8");
				} catch (UnsupportedEncodingException e) {
					System.err.println("Missing library UTF-8.\n");
				}
				totalLength += nick.length+1;
				pdu.extendTo(headerLength + totalLength);
				pdu.setSubrange(position, nick);
				position = headerLength + totalLength;
			}
		}
		pdu.setShort(2, (short)totalLength);
		pdu.extendTo(headerLength + totalLength + calcPadding(headerLength + totalLength));
		return pdu.getBytes();
	}

	public static byte[] findUser(ArrayList<User> users,String request) {
		PDU pdu = new PDU(12);
		pdu.setByte(0, (byte)OpCodes.UINFO);
		pdu.setByte(1, (byte)0);
		pdu.setShort(2, (byte)0);
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getNickname().equals(request)) {
				pdu.setSubrange(4,users.get(i).getIpAddress());
				pdu.setInt(8, users.get(i).getTimeJoined());
			}
		}
		return pdu.getBytes();
	}

	/**
	 * Gets Unix time in seconds
	 * @return
	 */
	public static int makeTimestamp() {
		long unixTime = (System.currentTimeMillis() / 1000L);
		int timeStamp = (int) unixTime;
		return timeStamp;
	}

	/**
	 * Calculates the amount of padding required to make the byte array evenly divisible by 4.
	 * @return
	 */
	private static int calcPadding(int size) {
		return (4 - (size % 4))%4;
	}

	public static short getUnsigned(byte b) {
		return (short) (b & 0xFF);
	}
	public static int getUnsigned(short b) {
		return (int) (b & 0xFFFF);
	}
	public static long getUnsigned(int b) {
		return (long) (b & 0xFFFFFFFF);
	}

}
