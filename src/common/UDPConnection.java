package common;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * UDPConnection.java
 *
 * The UDPConnection deals primarily with the communication between the client/server and the nameserver.
 * Its sole function is to send and receive packets.
 */

public class UDPConnection {

	private InetAddress remote;
	private int port;


	public UDPConnection(String remote, int port) throws UnknownHostException {
		this.remote = InetAddress.getByName(remote);
		this.port = port;
	}

	/**
	 * Send will receive an array of bytes, package it and then send it away to the nameserver.
	 * Afterwards it will return the answer received from the namerserver.
	 * @param sendData - An array of bytes to be sent away to the namerserver.
	 * @return receiveData - Data received as a response from the nameserver.
	 * @throws IOException
	 */

	public byte[] send(byte[] sendData) throws IOException {

		byte[] receiveData = new byte[1024];

		DatagramSocket socket = new DatagramSocket();
		socket.setSoTimeout(3000);
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, remote, port);
		socket.send(sendPacket);
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		try {
			socket.receive(receivePacket);
			receiveData = receivePacket.getData();
		} catch (SocketTimeoutException e) {
			receiveData = null;
		}
		socket.close();
		return receiveData;
	}

}
